
import pygame
from pygame import *
from pygame.locals import *
from player import Player
from blocks import Platform

WIDTH = 800
HEIGHT = 640
DISPLAY = (WIDTH, HEIGHT)
BACKGROUND = '#004400'
PT_WIDTH = 32
PT_HEIGHT = 32
PT_COLOR = '#ff6262'


class Camera(object):
    def __init__(self, camera_func, width, height):
        self.camera_func = camera_func
        self.state = Rect(0, 0, width, height)

    def apply(self, target):
        return target.rect.move(self.state.topleft)

    def update(self, target):
        self.state = self.camera_func(self.state, target.rect)


def camera_configure(camera, target_rect):
    l, t, _, _ = target_rect
    _, _, w, h = camera
    l, t = -l+WIDTH / 2, -t+HEIGHT / 2

    l = min(0, l)
    l = max(-(camera.width - WIDTH), l)
    t = max(-(camera.height - HEIGHT), t)
    t = min(0, t)

    return Rect(l, t, w, h)


def main():
    pygame.init()
    screen = pygame.display.set_mode(DISPLAY)

    bg = Surface((WIDTH, HEIGHT))
    hero = Player(100, 100)
    left = right = False
    up = False
    entities = pygame.sprite.Group()
    platforms = []
    entities.add(hero)
    level = [
        "----------------------------------",
        "-                                -",
        "-                       --       -",
        "-                                -",
        "-            --                  -",
        "-                                -",
        "--                               -",
        "-                                -",
        "-                   ----     --- -",
        "-                                -",
        "--                               -",
        "-                                -",
        "-                            --- -",
        "-                                -",
        "-                                -",
        "-      ---                       -",
        "-                                -",
        "-   -------         ----         -",
        "-                                -",
        "-                         -      -",
        "-                            --  -",
        "-                                -",
        "-                                -",
        "----------------------------------"]

    timer = pygame.time.Clock()
    bg.fill(Color(BACKGROUND))

    x = y = 0
    for row in level:
        for col in row:
            if col == '-':
                pf = Platform(x, y)
                entities.add(pf)
                platforms.append(pf)

            x += PT_WIDTH
        y += PT_HEIGHT
        x = 0

    total_level_width = len(level[0]) * PT_WIDTH
    total_level_height = len(level) * PT_HEIGHT

    camera = Camera(camera_configure, total_level_width, total_level_height)
    while True:
        timer.tick(60)

        for e in pygame.event.get():
            if e.type == QUIT: pygame.quit()
            if e.type == KEYDOWN and e.key == K_LEFT: left = True
            if e.type == KEYDOWN and e.key == K_RIGHT: right = True
            if e.type == KEYUP and e.key == K_LEFT: left = False
            if e.type == KEYUP and e.key == K_RIGHT: right = False

            if e.type == KEYDOWN and e.key == K_UP: up = True
            if e.type == KEYUP and e.key == K_UP: up = False

        screen.blit(bg, (0, 0))
        camera.update(hero)
        hero.update(left, right, up, platforms)
        for e in entities:
            screen.blit(e.image, camera.apply(e))
        pygame.display.update()


if __name__ == '__main__': main()





