from pygame import *

PT_WIDTH = 32
PT_HEIGHT = 32
PT_COLOR = '#ff6262'


class Platform(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.image = Surface((PT_WIDTH, PT_HEIGHT))
        self.image = image.load('blocks/platform.png')
        self.rect = Rect(x, y, PT_WIDTH, PT_HEIGHT)